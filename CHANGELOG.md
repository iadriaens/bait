**date**: November 21, 2022
**author**: Ines Adriaens

## Overview tools for changepoint analysis

**Ruptures**
------------

[documentation](https://centre-borelli.github.io/ruptures-docs/)  
[user guide](https://centre-borelli.github.io/ruptures-docs/user-guide/)  
[code](https://github.com/deepcharles/ruptures)  
[manuscript](http://www.laurentoudre.fr/publis/TOG-SP-19.pdf)

- exact and approximate changepoint detection for segmentation of non-stationary signals;
- PELT available (Pruned Exact Linear Time) 
- different cost functions (mean, median, mean/cov, linear, etc) available
- not too well documented / explained
- works with multivariate data, but not well documented
- still active


**sdt-python**
--------------

[documentation](https://schuetzgroup.github.io/sdt-python/index.html)  
[code](https://github.com/schuetzgroup/sdt-python)  
[zenodo, doi](https://zenodo.org/record/7078957#.Y3uN1XbMKUk)

- developed for processing of fluorescence microscopic images
- more restricted use for 
- PELT available, also Bayesian offline and online methods
- last update 2020



**Changepy**
------------

[code](https://github.com/ruipgil/changepy)  
[R package](https://github.com/rkillick/changepoint/)

- PELT alone implemented
- last update in 2017
- gateway for the R package rKillick cpt
- restricted number of cost functions available, incl. mean, var, meanvar, poisson and exponential


--------------------------------------------------------------

### Choice

**ruptures**