# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 09:55:26 2022

@author: adria036
"""

import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\bait\scripts\bait\datapreparation\harmen") 


#%% import packages

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt




#%% prepare dataset 2: accelerations (x,y,z)  sheep

path = os.path.join("W:","/ASG","WLR_Dataopslag",
                    "Genomica","Sustainable_breeding",
                    "4400003586_BAIT","Dataset_HD","Sheep2_ear")
svpath = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results")
# fn = "\\acceleration_harmen.tif"
# refdate = pd.to_datetime("today")

data = pd.DataFrame([])
for f in os.listdir(path):
    print(f)
    new = pd.read_csv(path+"\\"+f)
    new = new.drop(new.index.values[-1],axis=0)
    new["t"] = new.index.values
    new["at"] = pd.to_datetime(new["datetime"],format = "%d-%m-%Y %H:%M:%S.%f")
    """
    28/10/2022 : start recording 16:30
    04/11/2022 : remove data between 13:00 and 14:00 - keep only data from 1 accelerometer
    11/11/2022 : remove data between 14:00 and 15:00 - keep only data from 1 accelerometer
    """
    if "wk1_20221028" in f:
        new = new.loc[new["at"].dt.hour >= 17,:].reset_index(drop=1)
    elif "wk1_20221104" in f:
        new = new.loc[new["at"].dt.hour < 13,:].reset_index(drop=1)
        # fill gap with nan data
        idx = np.linspace(new.index.values[-1]+1,new.index.values[-1]+60*25*60,new.index.values[-1]+60*25*60-new.index.values[-1]).astype(int)
        refdate = pd.to_datetime("2022/11/04 13:00:00.000",format = "%Y/%m/%d %H:%M:%S.%f")
        df = pd.DataFrame([],index=idx,columns=new.columns)
        df["at"] = refdate + np.linspace(0,len(idx)-1,len(idx))* pd.Timedelta(40,"milli")
        df["t"] = idx
        df["datetime"] = df["at"].dt.strftime("%d-%m-%Y %H:%M:%S.%f")
        # add "gap" to new
        new = pd.concat([new,df],axis = 0)
    elif "wk2_20221104" in f:
        new = new.loc[new["at"].dt.hour >= 14,:].reset_index(drop=1)
    elif "wk2_20221111" in f:
        new = new.loc[new["at"].dt.hour < 14,:].reset_index(drop=1)
        # fill gap with nan data
        idx = np.linspace(new.index.values[-1]+1,new.index.values[-1]+60*25*60*2,new.index.values[-1]+60*25*60*2-new.index.values[-1]).astype(int)
        refdate = pd.to_datetime("2022/11/11 14:00:00.000",format = "%Y/%m/%d %H:%M:%S.%f")
        df = pd.DataFrame([],index=idx,columns=new.columns)
        df["at"] = refdate + np.linspace(0,len(idx)-1,len(idx))* pd.Timedelta(40,"milli")
        df["t"] = idx
        df["datetime"] = df["at"].dt.strftime("%d-%m-%Y %H:%M:%S.%f")
        # add "gap" to new
        new = pd.concat([new,df],axis = 0)
    elif "wk3_20221111" in f:
        new = new.loc[new["at"].dt.hour >= 16,:].reset_index(drop=1)
    elif "wk3_20221124" in f:
        new = new.loc[new["at"].dt.hour < 10,:].reset_index(drop=1)
    # preprocess
    new["win"] = np.floor(new["t"]/25)
    # prepare frame with datetimes aggregated to one second
    test2 = new["win"].drop_duplicates() # keep first
    test3 = new.iloc[test2.index.values,:]  # select data of first
    test3.index = test3.win    # change index to win
    accum = new[["win","acc_x","acc_y","acc_z"]].groupby(by="win").mean().reset_index()  # calculate mean activity
    test = accum.join(test3[["at","win"]], on= "win", rsuffix = "_m")  # join activity with date in at
    test = test.drop(columns = "win_m") 
    data = pd.concat([data,test])

# sort data based on date + add id
data = data.sort_values(by = "at").reset_index(drop=1)
data["id"] = 2

# smooth with rolling median 10 days
data["acc_xm"] = data["acc_x"].rolling(10).median() 
data["acc_ym"] = data["acc_y"].rolling(10).median() 
data["acc_zm"] = data["acc_z"].rolling(10).median() 



# # plot
# fig,ax = plt.subplots(nrows=3,ncols=1,figsize = (20,10),sharex=True)
# data2 = data.loc[data["at"].dt.day==1,:].sort_values(by="at").reset_index(drop=1)
# ax[0].plot(data2["at"],data2["acc_x"],color = "teal")
# ax[1].plot(data2["at"],data2["acc_y"],color = "teal")
# ax[2].plot(data2["at"],data2["acc_z"],color = "teal")
# ax[0].plot(data2["at"],data2["acc_xm"],color = "k")
# ax[1].plot(data2["at"],data2["acc_ym"],color = "k")
# ax[2].plot(data2["at"],data2["acc_zm"],color = "k")
# ax[0].plot(data2["at"],data2["acc_xm2"],color = "b", ls = "--")
# ax[1].plot(data2["at"],data2["acc_ym2"],color = "b", ls = "--")
# ax[2].plot(data2["at"],data2["acc_zm2"],color = "b", ls = "--")


#%% plot and save
data["day"] = data["at"].dt.day
data["month"] = data["at"].dt.month
days = data["day"].drop_duplicates()
for day in days:
    print(day)
    month = data.loc[data["day"]==day,"at"].dt.month.drop_duplicates().reset_index(drop=1)
    fn = "sheep2ear" + "_2022" + str(month[0]) + str(day) + ".png"
    fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
    ax.plot(data.loc[data["day"]==day,"at"],data.loc[data["day"]==day,["acc_x","acc_y","acc_z"]])
    ax.plot(data.loc[data["day"]==day,"at"],data.loc[data["day"]==day,["acc_xm","acc_ym","acc_zm"]],
            color = "k", linewidth = 0.5)
    ax.set_title("sheep 2 ear - " + str(day) + "/" + str(month[0]) + '/2022')
    ax.set_xlabel("time")
    ax.set_ylabel("acceleration in m/s²")
    plt.savefig(svpath + "\\" + fn)
    plt.close()

# save
path = r'C:/Users/adria036/OneDrive - Wageningen University & Research/iAdriaens_doc/Projects/iAdriaens/bait/data/preprocessed'
data.to_csv(path + "\\HDdata_sheep2_ear.csv")
