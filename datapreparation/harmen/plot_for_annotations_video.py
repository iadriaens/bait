# -*- coding: utf-8 -*-
"""
Created on Wed Dec 14 09:56:54 2022

@author: adria036
------------------------------------------------------------------------------

Code to load and visualise data to check with video

------------------------------------------------------------------------------


"""

# load packages, change file path

import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\bait\scripts\bait\tool") 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# magic statement to plot in zoomable frame - run separately, or change .py to .ipy
# %matplotlib qt

#%% load data

# set path to data
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","segmentation")
fn = "\\HD_sheep1_segmented_2min.csv"

# load data with segments  - data contains:
#   - acceleration median filtered 10s
#   - segment no = segment
#   - datetime stamp = at
#   - date2 = time stamp shortend for plotting

data = pd.read_csv(path + "//HD_sheep1_segmented.csv", index_col=0)
data["at"] =  pd.to_datetime(data["at"],format = "%Y-%m-%d %H:%M:%S")
data["date2"] = data["at"].dt.strftime("%d/%m %H:%M:%S")


#%% set & print check dates

#-----------------------------------------------------------------------------

# check range - calculate 
start_check = pd.to_datetime("2022-10-29 11:02:05", format = "%Y-%m-%d %H:%M:%S")
end_check = pd.to_datetime("2022-10-29 11:08:22", format = "%Y-%m-%d %H:%M:%S")

# set start of video
video_start = pd.to_datetime("2022-10-28 13:18:00", format = "%Y-%m-%d %H:%M:%S")

#-----------------------------------------------------------------------------


# start & endtime of video
days, seconds = (start_check - video_start).days, (start_check - video_start).seconds
if days != 0:
    print("Wrong video")
else:
    hours = str(days * 24 + seconds // 3600)
    minutes = str((seconds % 3600) // 60)
    seconds = str(seconds % 60)
    if len(seconds) < 2:
        seconds = '0'+seconds
    vidtime_start = hours + ":" + minutes + ":" + seconds
    # endtime
    days, seconds = (end_check - video_start).days, (end_check - video_start).seconds
    hours = str(days * 24 + seconds // 3600)
    minutes = str((seconds % 3600) // 60)
    seconds = str(seconds % 60)
    if len(seconds) < 2:
        seconds = '0'+seconds
    vidtime_end = hours + ":" + minutes + ":" + seconds
    # print check dates              
    print("\n\n Check video from " + vidtime_start + " to " + vidtime_end)


del days, hours, minutes, seconds

#%% plot in predefined range

#-----------------------------------------------------------------------------

# plot accelerometer data
startdate = pd.to_datetime("2022-10-29 10:00:00", format = "%Y-%m-%d %H:%M:%S")
enddate = pd.to_datetime("2022-10-29 12:00:00", format = "%Y-%m-%d %H:%M:%S")
sheepid = 1

#-----------------------------------------------------------------------------

# initiate figure
fig, ax = plt.subplots(nrows=3, ncols = 1, figsize = (18,9), sharex = True)
data2 = data.loc[(data["at"]>= startdate) & (data["at"]<= enddate),:].sort_values(by="at").reset_index(drop=1)

ax[0].plot(data2["at"],data2["acc_xm"],color = "teal")
ax[1].plot(data2["at"],data2["acc_ym"],color = "teal")
ax[2].plot(data2["at"],data2["acc_zm"],color = "teal")

cols = np.tile(["red","blue"],len(data2["segment"].drop_duplicates()/2))
T = 0
for segment in data2["segment"].drop_duplicates():
    subset = data2.loc[data2["segment"] == segment,["at","segment"]].drop_duplicates().reset_index(drop=1)
    ax[0].text(subset["at"][0],0.8,round(segment),fontsize = "xx-small")
    ax[0].fill_between(subset["at"], 
                       np.ones_like(subset["segment"])*data["acc_xm"].min(),
                       np.ones_like(subset["segment"])*data["acc_xm"].max(), 
                       color = cols[T],alpha=0.2,edgecolor = "k")
    ax[1].fill_between(subset["at"], 
                       np.ones_like(subset["segment"])*data["acc_ym"].min(),
                       np.ones_like(subset["segment"])*data["acc_ym"].max(), 
                       color = cols[T],alpha=0.2,edgecolor = "k")
    ax[2].fill_between(subset["at"], 
                       np.ones_like(subset["segment"])*data["acc_zm"].min(),
                       np.ones_like(subset["segment"])*data["acc_zm"].max(), 
                       color = cols[T],alpha=0.2,edgecolor = "k")
    T=T+1


ax[0].set_title("from " + str(startdate)  + " to " + str(enddate))
ax[2].set_xlabel("datetime")
ax[0].set_ylabel("acc_x [m/s²]")
ax[1].set_ylabel("acc_y [m/s²]")
ax[2].set_ylabel("acc_z [m/s²]")

