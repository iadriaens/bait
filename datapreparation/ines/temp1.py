# -*- coding: utf-8 -*-
"""
Created on Tue Nov 15 09:06:43 2022

@author: adria036
"""


import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\bait\scripts\bait\datapreparation\ines") 


#%% import packages

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import date,timedelta

# define functions

def accel_act(df,window):
    """
    combine the accelarations in the x,y and z dimension into a general activity
    paramter.
    Step 1 : rescale values in each direction
    Step 2 : set level of activtiy based on amplitude in preset window
    Step 3 : combine activity level in 3 directions
    
    df: Pandas DataFrame with columns 'acc_x', 'acc_y', 'acc_z','id', 'at'
    window: 
    
    """
    try:
        df = df.rename(columns = {"cowid":"id"})
        namechange = 1
    except:
        namechange = 0
        
    # calculate activity
    ids = df["id"].drop_duplicates().reset_index(drop=1)
    output = pd.DataFrame([])
    for subj in ids:
        print("Calculating activity for subj = " + str(int(subj)))
        sset = df.loc[(df["id"]==subj) & \
                      (~df["acc_x"].isna()) & \
                      (~df["acc_y"].isna()) & \
                      (~df["acc_z"].isna()),:].copy()
        
        # standardise and scale measurements
        sset["acc_xs"] = (sset["acc_x"]-sset["acc_x"].min())/(sset["acc_x"].max()-sset["acc_x"].min())
        sset["acc_ys"] = (sset["acc_y"]-sset["acc_y"].min())/(sset["acc_y"].max()-sset["acc_y"].min())
        sset["acc_zs"] = (sset["acc_z"]-sset["acc_z"].min())/(sset["acc_z"].max()-sset["acc_z"].min())
        
        # set datetime to days, seconds, microseconds
        sset["d"] = sset["at"].dt.day-sset["at"].dt.day.min()
        sset["s"] = sset["at"].dt.hour*3600 \
                         + sset["at"].dt.minute*60 \
                         + sset["at"].dt.second
        # sset["ms"] = sset["at"].dt.microsecond/100
        
        # accumulated activity calculated in window: set window index
        sset["win"] = np.floor(sset["s"]/window)
        
        # accumulate activity in set windowsize - operation = std
        act = sset[["d","win","acc_xs","acc_ys","acc_zs"]].groupby(by=["d","win"]).std()
        act["acc_xs"] = act["acc_xs"].fillna(act["acc_xs"].mean()) # if only 1 measurement std= nan
        act["acc_ys"] = act["acc_ys"].fillna(act["acc_ys"].mean()) # replace by mean std
        act["acc_zs"] = act["acc_zs"].fillna(act["acc_zs"].mean())
        act["activity"] = (act["acc_xs"]+act["acc_ys"]+act["acc_zs"])/3
        act = act.reset_index()
        sset = pd.merge(sset,act[["d","win","activity"]],on = ["d","win"])
        
        
        # plot activity, standardized activity and time series of result
        fig,ax = plt.subplots(nrows=1,ncols=3,figsize = (20,10))
        ax[0].hist(sset["acc_x"],density=True,color = "g")
        ax[1].hist(sset["acc_y"],density=True,color = "g")
        ax[2].hist(sset["acc_z"],density=True,color = "g")
        fig,ax = plt.subplots(nrows=1,ncols=3,figsize = (20,10))
        ax[0].hist(sset["acc_xs"],density=True,color = "g")
        ax[1].hist(sset["acc_ys"],density=True,color = "g")
        ax[2].hist(sset["acc_zs"],density=True,color = "g")
        fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
        T = 0
        for d in act["d"].drop_duplicates():
            ax.plot(act.loc[act["d"]==d,"win"],act.loc[act["d"]==d,"activity"]+T)
            T+=0.5
        ax.set_title("activity for subject " + str(subj))
        ax.set_xlabel("time of the day, window = " + str(window) + "s")
        ax.set_ylabel("activity")
        
        # combine data from all cows
        output = pd.concat([output,sset],axis = 0)
        
        # change name back to initial name
        if namechange == 0:
            output = output.rename(columns = {"id":"cowid"})
            
    return output 
  


#%% prepare dataset 1: accelerations (x,y,z) DC cows
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","data","nlas_ines")

# read data
data = pd.DataFrame([])
for f in os.listdir(path) :
    if ("bait" in f):
        print(f)
        
        new = pd.read_csv(path+"\\"+f,usecols=[0,4,7,8,9,10,11,14,16,18,19,20])
        new["at"] = pd.to_datetime(new["at"],format = "%Y-%m-%d %H:%M:%S.%f%z")
        
        # select animals list of cows with single sensor in barn 72 (no replacements)
        cows = pd.DataFrame([3152,1681,821,3106,419,3089,7387,1091,605],columns = ["cowid"])
        new = pd.merge(new,cows,how="inner")
        
        # add to previous dataset
        data = pd.concat([data,new],axis=0)
    
del new, path, cows, f

# visualisation of data: acceleration in x,y,z
fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,18))
T = 0
for ID in data["cowid"].drop_duplicates():
    ax.plot(data.loc[data["id"]==ID,"t"],data.loc[data["id"]==ID,"acc_x"]+T,linewidth=0.25,color="steelblue")
    ax.plot(data.loc[data["id"]==ID,"t"],data.loc[data["id"]==ID,"acc_y"]+T,linewidth=0.25,color="teal")
    ax.plot(data.loc[data["id"]==ID,"t"],data.loc[data["id"]==ID,"acc_z"]+T,linewidth=0.25,color="limegreen")
    T+=data[["acc_x","acc_y","acc_z"]].max().max()+1
ax.set_title("activity")
ax.set_xlabel("t")
ax.set_ylabel("acceleration")
ax.legend(["acc_x","acc_y","acc_z"])

# calculate general activity level
df = data[["cowid","acc_x","acc_y","acc_z","at"]]
window = 60  # in seconds
df2 = accel_act(df,window)

# add activity to data
data = pd.merge(data,df2[["cowid","at","activity"]],on = ["cowid","at"],how="outer")
del window, df, df2


# reformat as preferred for segmentation module (strip)




#%% prepare dataset 2: accelerations (x,y,z) 

path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","data","nlas_harmen")

refdate = pd.to_datetime("today")

# read data
data = pd.DataFrame([])
ID = 1
for f in os.listdir(path):
    print(f)
    
    # read data and reformat
    new = pd.read_csv(path+"\\"+f,header = None)
    new.columns = ["acc_x","acc_y","acc_z"]
    new["id"] = ID
    ID += 1
    new["t"] = range(0,len(new))
    ts = np.linspace(0,len(new),len(new))* pd.Timedelta(40,"milli")
    new["at"] = refdate + ts
    new = new[["id","at","t","acc_x","acc_y","acc_z"]]
    
    # add to previous dataset
    data = pd.concat([data,new],axis=0)
    
del f, new, path, ID, ts, refdate

# visualise data - acceleration in x,y,z
fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
T = 0
for ID in data["id"].drop_duplicates():
    ax.plot(data.loc[data["id"]==ID,"t"],data.loc[data["id"]==ID,"acc_x"]+T,linewidth=0.25,color="steelblue")
    ax.plot(data.loc[data["id"]==ID,"t"],data.loc[data["id"]==ID,"acc_y"]+T,linewidth=0.25,color="teal")
    ax.plot(data.loc[data["id"]==ID,"t"],data.loc[data["id"]==ID,"acc_z"]+T,linewidth=0.25,color="limegreen")
    T+=data[["acc_x","acc_y","acc_z"]].max().max()+1
ax.set_title("activity")
ax.set_xlabel("t")
ax.set_ylabel("acceleration")
ax.legend(["acc_x","acc_y","acc_z"])
del T, fig,ax

# calculate general activity level
df = data[["id","acc_x","acc_y","acc_z","at"]]
window = 60  # in seconds
df2 = accel_act(df,window)

# add activity to data
data = pd.merge(data,df2[["id","at","activity"]],on = ["id","at"],how="outer")

# clear workspace
del window, df, df2





#%% reformat as preferred for segmentation module (strip)