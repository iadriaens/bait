# -*- coding: utf-8 -*-
"""
Created on Mon Dec 19 09:25:15 2022

@author: adria036

-------------------------------------------------------------------------------
Create data for supervised learning from area in segments



"""
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\bait\scripts\bait\datapreparation\ines") 

import pandas as pd
import barn_areas

#%% load data
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","segmentation")

# segments
data = pd.read_csv(path + "//IA_cows_segmented_3min.csv", index_col=(0))
data["id"].drop_duplicates()
data["at"] = pd.to_datetime(data["at"],format = "%Y-%m-%d %H:%M:%S.%f%z")


# raw
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","data","preprocessed")
pdata = pd.read_csv(path + "//DCdata.csv", index_col = (0))
pdata["at"] = pd.to_datetime(pdata["at"],format = "%Y-%m-%d %H:%M:%S.%f%z")

del path

# area calculation based on X,y

data_area, parea = barn_areas.assign_behaviour(pdata["xnew"],pdata["ynew"])
data_area = data_area[~data_area.index.duplicated(keep='first')]
data_area["no"].drop_duplicates()


newdata = pd.concat([pdata,data_area["no"]],axis=1)
newdata = newdata.rename(columns = {"cowid":"id"})

# combine newdata with segmented data based on timestamp and id/cowid
test = pd.concat([data,newdata[["id","at","no"]]],
                 join="inner",
                 axis=1,
                 ignore_index = True,
                 keys = ["id","at"]
                 )

data2 = test.drop(columns = [7,8]).rename(columns = {0:"acc_x",1:"acc_y",2:"acc_z",3:"segment",4:"at",5:"date2",6:"id",9:"no"})


svpath = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results")



#%% load segm