# -*- coding: utf-8 -*-
"""
Created on Mon Dec 19 17:03:44 2022

@author: adria036
"""
#%% import packages

import pandas as pd
# from datetime import  date,timedelta
import numpy as np
# from read_data_sewio import start_end_dates, read_all_data, read_ids, combine_data_id
import os
import seaborn as sns
import matplotlib as plt


#%% visualisation functions

def heatmap_barn(data_x,data_y,xstep,ystep,x_lim,y_lim):
    """
    Plot the frequency of positions registered, given data_x and data_y
    xstep and ystep determine how fine these positions are taken
    x_lim and y_lim set boundaries (e.g. incl feeding lane)
    
    Parameters
    ----------
    data_x : TYPE = Pandas series
        Data of the x-coordinate as registered by the sewio system
    data_y : TYPE = Pandas series
        Data of the y-coordinate as registered by the sewio system
    xstep : TYPE = float
        How fine you want to determine location in x-direction (e.g. 1 is one
            meter, 0.1 = 10 centimeters)
    ystep : TYPE = float
        How fine you want to determine location in x-direction (e.g. 1 is one
            meter, 0.1 = 10 centimeters)
    x_lim : TYPE = list of floats
        Physical boundaries of plot you want to make (e.g. cut off feeding lane
            then indicate lower boundary in y direction = -4)
    y_lim : TYPE = list of floats
        Physical boundaries of plot you want to make (e.g. cut off feeding lane
            then indicate lower boundary in y direction = -4)

    Returns
    -------
    ax : TYPE = sns heatmap
        figure axes with heatmap

    """
    # transform data_x and data_y for count
    x_transf = (data_x*(1/xstep)).round()*xstep
    y_transf = (data_y*(1/ystep)).round()*ystep
    
    # put x_transf and y_transf in DataFrame
    frame = {'xT' : x_transf.round(decimals = 4),
             'yT' : y_transf.round(decimals = 4) }
    df = pd.DataFrame(frame)
    
    # count and groupby
    heatdata = df.groupby(["xT","yT"]).size()
    heatdata = heatdata.to_frame(name = "counts")
    heatdata = heatdata.reset_index()
    
    # set limits to agree with steps
    x_low = round(round(x_lim[0]*(1/xstep))*xstep,ndigits = 4)
    x_high = round(round(x_lim[1]*(1/xstep))*xstep,ndigits = 4)
    y_low = round(round(y_lim[0]*(1/ystep))*ystep,ndigits = 4)
    y_high = round(round(y_lim[1]*(1/ystep))*ystep,ndigits = 4)
    
    # delete the rows for which values are outside x_lim or y_lim
    heatdata = heatdata.loc[(heatdata["xT"] >= x_lim[0]) &
                 (heatdata["xT"] <= x_lim[1]) &
                 (heatdata["yT"] >= y_lim[0]) &
                 (heatdata["yT"] <= y_lim[1])]
    heatdata.reset_index(drop = True, inplace = True)
    
    # expected axes limits based on lim and step
    x = np.arange(x_low,x_high+xstep,xstep,dtype = float)
    y = np.arange(y_low,y_high+ystep,ystep,dtype = float)
    
    # fill array
    heat_all = []
    for i in y:
        for ii in x:
            number_values = heatdata.loc[(heatdata["xT"] == round(ii,ndigits = 4)) &
                                         (heatdata["yT"] == round(i,ndigits = 4)),
                                         ["counts"]]
            if number_values.empty:
                number_values = pd.DataFrame(data = [0], columns = ["counts"])
            number_values = number_values.reset_index(drop=1)
            # print(round(i,ndigits = 4),round(ii,ndigits = 4),number_values["counts"][0])
            heat_all.append(number_values["counts"][0])
            
        
    # make rectangular and convert list to numpy
    heat_all = np.array(heat_all)
    heat_all = np.reshape(heat_all, [len(y),len(x)])
    heat_all = heat_all.transpose()
    test = np.flip(heat_all, axis = 1)
    
    
    # convert to df with right colnames and indices
    xcols = np.array(x.round(decimals=3),dtype = str)
    ycols = np.flip(np.array(y.round(decimals=3),dtype = str))
    df = pd.DataFrame(test, columns = ycols, index = xcols)
    
    # make heatmap
    ax = sns.heatmap(df,
                     yticklabels = round(1/xstep),
                     xticklabels = round(1/ystep*2),
                     robust = True
                     )

    return ax

#%% usage
# ------------------------- individual cow heatmaps ---------------------------
# unique cows
data_area['id'] = 1
cows = data_area.loc[:,["id"]].drop_duplicates() 
cows = cows.sort_values(by = ["id"]).reset_index(drop=1)


data_x = data_area.X
data_y = data_area.y


# unique days
days = data2["at"].dt.day.drop_duplicates() 

# settings heatmap
xstep = 0.1
ystep = 0.1

# plots per cow-day
for cow in cows["id"]:
    # select data and create heatmaps
    for dd in days:
        print(cow, dd)
        # select data
        data_x = data2.loc[(data2["id"] == cow) &
                          (data2["at"].dt.day == dd),"xnew"]
        data_y = data2.loc[(data2["id"] == cow) &
                          (data2["at"].dt.day == dd),"ynew"]
        
        # set limits (based on information barn)
        x_lim = [data_x.min(),data_x.max()]#[0, 11] #
        y_lim = [data_y.min(),data_y.max()]#[-19,0] #[data_y.min(),data_y.max()]
        
        # plot heatmap
        plt.rcdefaults() 
        fig, ax = plt.pyplot.subplots() 
        ax = heatmap_barn(data_x, data_y, xstep, ystep, x_lim, y_lim)
        ax.set_xlabel('y coordinate')
        ax.set_ylabel('x coordinate')
        ax.set_title('cow = ' + str(cow) + ', date = ' + dd.replace("-",""))
        ax.xaxis.tick_top()
        plt.pyplot.savefig(path_out + "//heatmap_" + str(cow) + "_" + dd.replace("-",""))


