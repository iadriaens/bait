# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29, 2022

@author: adria036

-------------------------------------------------------------------------------

Data preparation script -   Collected via NLAS script
Dates : between April to August, 2022
Location: at Dairy Campus, Leeuwarden
Sensors: UWB positioning (spatial data: X, Y) + Accelerations (X, Y, Z)
1) Activity measured by accelerometers in 3 dimensions
2) 

-------------------------------------------------------------------------------

Remarks:
    - corrected for time-desynchronization with UTC
    - activity based on UWB reference with thresholds:
        ° distance moved
        ° speed
        ==> into categories of activity (very to little ~ behaviours)
    - activity measured by accelerometers in 3 dimensions


-------------------------------------------------------------------------------
file paths - to dataopslag on the WUR W: drive

"""
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\bait\scripts\bait\datapreparation\ines") 




#%% import packages

import os
from datetime import date, timedelta
import pandas as pd
import numpy as np
import copy
from filter_data import filter_barn_edges, filter_interp
from read_data_sewio import read_all_data, read_ids, combine_data_id
import barn_areas
import matplotlib.pyplot as plt
import seaborn as sns
# import sqlite3

def accel_act(df,window):
    """
    combine the accelarations in the x,y and z dimension into a general activity
    paramter.
    Step 1 : rescale values in each direction
    Step 2 : set level of activtiy based on amplitude in preset window
    Step 3 : combine activity level in 3 directions
    
    df: Pandas DataFrame with columns 'acc_x', 'acc_y', 'acc_z','id', 'at'
    window: 
    
    """
    svpath = os.path.join("C:","/Users","adria036",
                        "OneDrive - Wageningen University & Research","iAdriaens_doc",
                        "Projects","iAdriaens","bait","results")
    
    try:
        df = df.rename(columns = {"cowid":"id"})
        namechange = 1
    except:
        namechange = 0
        
    # calculate activity
    ids = df["id"].drop_duplicates().reset_index(drop=1)
    output = pd.DataFrame([])
    for subj in ids:
        print("Calculating activity for subj = " + str(int(subj)))
        sset = df.loc[(df["id"]==subj) & \
                      (~df["acc_x"].isna()) & \
                      (~df["acc_y"].isna()) & \
                      (~df["acc_z"].isna()),:].copy()
        
        # standardise and scale measurements
        sset["acc_xs"] = (sset["acc_x"]-sset["acc_x"].min())/(sset["acc_x"].max()-sset["acc_x"].min())
        sset["acc_ys"] = (sset["acc_y"]-sset["acc_y"].min())/(sset["acc_y"].max()-sset["acc_y"].min())
        sset["acc_zs"] = (sset["acc_z"]-sset["acc_z"].min())/(sset["acc_z"].max()-sset["acc_z"].min())
        
        # set datetime to days, seconds, microseconds
        sset["d"] = sset["at"].dt.day-sset["at"].dt.day.min()
        sset["s"] = sset["at"].dt.hour*3600 \
                         + sset["at"].dt.minute*60 \
                         + sset["at"].dt.second
        # sset["ms"] = sset["at"].dt.microsecond/100
        
        # accumulated activity calculated in window: set window index
        sset["win"] = np.floor(sset["s"]/window)
        
        # accumulate activity in set windowsize - operation = std
        act = sset[["d","win","acc_xs","acc_ys","acc_zs"]].groupby(by=["d","win"]).std()
        act["acc_xs"] = act["acc_xs"].fillna(act["acc_xs"].mean()) # if only 1 measurement std= nan
        act["acc_ys"] = act["acc_ys"].fillna(act["acc_ys"].mean()) # replace by mean std
        act["acc_zs"] = act["acc_zs"].fillna(act["acc_zs"].mean())
        act["activity"] = (act["acc_xs"]+act["acc_ys"]+act["acc_zs"])/3
        act = act.reset_index()
        sset = pd.merge(sset,act[["d","win","activity"]],on = ["d","win"])
        
        
        # plot activity, standardized activity and time series of result
        # fig,ax = plt.subplots(nrows=1,ncols=3,figsize = (20,10))
        # ax[0].hist(sset["acc_x"],density=True,color = "g")
        # ax[1].hist(sset["acc_y"],density=True,color = "g")
        # ax[2].hist(sset["acc_z"],density=True,color = "g")
        # fig,ax = plt.subplots(nrows=1,ncols=3,figsize = (20,10))
        # ax[0].hist(sset["acc_xs"],density=True,color = "g")
        # ax[1].hist(sset["acc_ys"],density=True,color = "g")
        # ax[2].hist(sset["acc_zs"],density=True,color = "g")
        fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
        T = 0
        for d in act["d"].drop_duplicates():
            ax.plot(act.loc[act["d"]==d,"win"],act.loc[act["d"]==d,"activity"]+T)
            T+=0.5
        ax.set_title("activity for subject " + str(int(subj)))
        ax.set_xlabel("time of the day, step = " + str(window) + "s")
        ax.set_ylabel("activity per day")
        plt.savefig(svpath + "\\activity_" + str(int(subj))+ ".tif")
        
        # combine data from all cows
        output = pd.concat([output,sset],axis = 0)
        
        # change name back to initial name
        if namechange == 1:
            output = output.rename(columns = {"id":"cowid"})
            
    return output 

#%% set filepaths and dates

# set path to data X,y
path_data = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_sewio"
    )

# set path to sql data folder
path_sql = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_total"
    )

# set path to results folder
path_res = os.path.join("C:","/Users","adria036","OneDrive - Wageningen University & Research",
                        "iAdriaens_doc","Projects","iAdriaens","bait","data",
                        "nlas_ines/"
                        )

# set file name for cowid information
fn = "\Copy_of_Sewio_tags.xlsx"

# set start and end dates
startdate = date(2022,7,1)
enddate = date(2022,7,10)

# set parameters for the imputation and interpolation
win_med = 31
gap_thres = 180
    
# # clear workspace
# del fn, path_data


#%% data selection





t_interval = enddate - startdate
for dd in range(t_interval.days+1):
    dd_0 = timedelta(dd-1)
    dd = timedelta(dd)
    print(startdate+dd_0)
    
    # --------------------------read & select data-----------------------------
    try:
        data = read_all_data(startdate+dd_0,startdate+dd,path_data)
    
        # read id/tag data
        tags = read_ids(path_data, fn)

        # combine it with data
        data = combine_data_id(data,tags)
        data["date"] = data["at"].dt.date
        data = data.loc[data["date"] == startdate+dd,:]
        # add selection step to correct for time > only day "startdate + dd" retained
        data = data.loc[data["date"] == startdate+dd,:]
            # remaining tags without cowID = during the changing
            # remove those values
        data = data.loc[data["cowid"] != 0,:]
        data = data.sort_values(by=["cowid","at"]).reset_index(drop=1)

        
        data = data[["cowid","alias","barn","date","at","feed_reference","title","X","y","acc_x","acc_y","acc_z"]]    
        # unique 'seconds' in the dataset 
        #data["day"] = data["at"].dt.day - min(data["at"].dt.day)
        data["hour"] = data["at"].dt.hour
        data["minute"] = data["at"].dt.minute
        data["second"] = data["at"].dt.second
        data["relsec"] = data["hour"]*3600 \
                         + data["minute"]*60 \
                         + data["second"]
        nsec = data["relsec"]
        cowids = data["cowid"]
        df = pd.concat([cowids,nsec],axis = 1).drop_duplicates()  # gives the indices of unique first 
        del df["relsec"], df["cowid"]
        
        # gap in acceleration data
        data["gap"] = data["at"].diff()
        test = data["gap"]*1000
        data["gap"] = test.dt.seconds/1000
        del test
        data.loc[data["gap"] < 0,"gap"] = np.nan # set to nan when different cowid
        
        # innerjoin for selection step - unique seconds in the dataset
        data_pos = pd.concat([data,df],axis = 1, join = 'inner')
        data_pos["OID"] = data_pos.index.values
        data_pos = data_pos.sort_values(by = ["cowid","at"]).reset_index(drop=1)
        
        # unfiltered data - one per second or less, all cows included  
        # calculate gaps based on numeric time relative to start of dataset
        data_pos["gap"] = data_pos["relsec"].diff()
        data_pos.loc[data["gap"] < 0,"gap"] = np.nan # set to nan when different cowid

        #--------------------------edit barn edges---------------------------------
        # set data outside edges to edges x < 0 and < 10.6 or x < 21 and x > 32
        data_pos.loc[(data_pos["X"] < 15),"X"] = filter_barn_edges( \
                 copy.copy(data_pos.loc[(data_pos["X"] < 15),"X"]),0,10.6) #barn 72
        data_pos.loc[(data_pos["X"] >= 15),"X"] = filter_barn_edges( \
                  copy.copy(data_pos.loc[(data_pos["X"] >= 15),"X"]),21,32) # barn 70

        # set data outside edges to edges y < -18 or y > -2.5
        data_pos["y"] = filter_barn_edges(copy.copy(data["y"]),-18,-2.5)

        #data["X"].hist()
        #data["y"].hist()
        
        # delete rows without cowid
        data_pos = data_pos.loc[data["cowid"]>0,:]

        #-----------------------filter & interpolate per cow-----------------------
        # filter the (X,y) time series with a median filter and interpolate
        
        # cows in the barn
        cows = data_pos["cowid"].drop_duplicates().sort_values().reset_index(drop=1)
        cows = pd.DataFrame(cows)
        
        # loop over all data per cowid and select (x,y,t) data to filter
        result = pd.DataFrame({"cowid": [],
                               "date": [],
                               "t" : [],
                               "gap" :  [],
                               "X" :  [],
                               "y" :  [],
                               "xnew" : [],
                               "ynew" : [],
                               "area" : []})
        for i in range(0,len(cows)):
            # select data
            cow_OID = data_pos.loc[(data_pos["cowid"] == cows["cowid"][i]),["OID","relsec"]]
            cow_t = data_pos.loc[(data_pos["cowid"] == cows["cowid"][i]),"relsec"]
            cow_x = data_pos.loc[(data_pos["cowid"] == cows["cowid"][i]),"X"]
            cow_y = data_pos.loc[(data_pos["cowid"] == cows["cowid"][i]),"y"]
            
            # filter data
            if len(cow_t) > 10:
                # add edges if t doesn't end at second 86399 of the day
                if cow_t.max() < 86399:
                    cow_t = pd.concat([cow_t,pd.Series(86399)], axis=0)
                    cow_x = pd.concat([cow_x,pd.Series(np.nan)], axis=0)
                    cow_y = pd.concat([cow_y,pd.Series(np.nan)], axis=0)
                
                # add edges if t doesn't start with 0th second of the day
                if cow_t.iloc[0]/86400 - float(cow_t.iloc[0]//86400) > 0.00001:
                    cow_t = pd.concat([pd.Series(cow_t.iloc[0]//86400*86400),cow_t], axis=0)
                    cow_x = pd.concat([pd.Series(np.nan),cow_x], axis=0)
                    cow_y = pd.concat([pd.Series(np.nan),cow_y], axis=0)

                
                # filter
                df,x,y = filter_interp(cow_t,cow_x,cow_y,win_med,gap_thres)
                df = df.reset_index(drop=1)
                
                # asign barn areas
                data_area, parea = barn_areas.assign_behaviour(x,y)
                data_area = data_area[~data_area.index.duplicated(keep='first')]
                
                # merge with df
                df["area"] = data_area["no"]
                
                # select columns and add cowid
                df["cowid"] = cows["cowid"][i]
                df["date"] = str(startdate+dd)
                df = df[["cowid","date","t","gap","X","y","xnew","ynew","area"]]
                df["t"] = df["t"].astype(int)
                df = pd.merge(df,cow_OID,how="outer",left_on = "t",right_on = "relsec")
                df.loc[df["xnew"].isna(),"area"] = np.nan
            else:
                df = pd.DataFrame({"cowid":cows["cowid"][i],
                               "date":str(startdate+dd),
                               "t" : pd.Series(np.arange(0,86400,1)),
                               "gap" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "X" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "y" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "xnew" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "ynew" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "area" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "relsec" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "OID" : pd.Series(np.arange(0,86400,1)*np.nan)})
                
            # concat dataframes all cows together 
            result = pd.concat([result,df])
            
        # concatenate result with the accelation data on "OID"
        data["OID"] = data.index.values
        result = pd.merge(data,result[["t","xnew","ynew","area","relsec","OID"]],
                          how="outer",
                          left_on = ["OID","relsec"],
                          right_on = ["OID","relsec"])

        # save results
        datestr = str(startdate+dd)
        datestr = datestr.replace("-","")    
        fns =  datestr + '_bait.txt'
        result.to_csv(path_res + fns, index = False)
        
        # clear workspace and memory
        del data, df, datestr, fns, result, data_area, x, y, cow_t, cow_x, cow_y, parea, cows, nsec
    except:
        pass

    

#%% prepare annotated

path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","data","nlas_ines")
svpath = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","preprocessed","cow")

# list comprehension
fn = [f for f in os.listdir(path) if "bec" in f]
data = pd.DataFrame([])
for f in fn:
    print(f)
    new = pd.read_csv(path+ "\\" + f)
    data = pd.concat([data,new])
    
data.loc[data["xnew"].isna(),"area"] = np.nan

# select cows
cows = pd.DataFrame([3152,1681,821,3106,419,3089,7387,1091,605],columns = ["cowid"])
data = pd.merge(data,cows,how="inner")

cows = data.cowid.drop_duplicates()
for cow in cows:
    subset = data.loc[data["cowid"]==cow,["cowid","date","t","xnew","ynew","area"]]
    subset.loc[(subset["area"] == 2) | (subset["area"] == 3),"area"] = 1
    subset.loc[(subset["area"].isna()),"area"] = 7
    subset = subset[(subset.area != 6)]
    sumdata = subset[["date","area","cowid"]].groupby(by=["date","area"]).count().reset_index()
    sumdata["percentage"] = round(sumdata["cowid"]/864,2)
    sumdata2=sumdata.groupby(by="date").sum()
    
    
    #fig,ax = plt.subplots(nrows=1,ncols=1,figsize=(20,10))
    sns.set(style="ticks")
    sns.set_style("whitegrid")
    g = sns.catplot(data=sumdata,x="date",y = "percentage", col="area",kind="bar",height=4,aspect=0.7,palette = 'magma')
    #ax.legend(["walk","cub_a","cub_b","cub_c","feed","drink","conc"])
    g.set_axis_labels("", "Percentage area usage")
    g.set_xticklabels(sumdata.date.drop_duplicates(),rotation=45,horizontalalignment='right')
    g.col_names = ['walk','cubicle','feed','drink','unknown']
    g.set_titles("{col_name}")
    g.fig.suptitle("cow " + str(cow))
    g.fig.subplots_adjust(top = 0.85)
    g.savefig(svpath + "\\timebudgets"+str(cow)+".png")



#%% join with segmented data

path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","segmentation")
fn = "\\IA_cows_segmented_3min2.csv"

sdata = pd.read_csv(path+fn,index_col = 0)
sdata["at"] = pd.to_datetime(sdata["at"],format = "%Y-%m-%d %H:%M:%S.%f%z")
sdata["day"] = sdata["at"].dt.day
sdata["t"] = sdata["at"].dt.second + \
             sdata["at"].dt.minute * 60 + \
             sdata["at"].dt.hour * 3600
sdata["cowid"] = sdata["id"]
sdata["date"] = sdata["at"].dt.strftime("%Y-%m-%d")

# change areas to supposed behaviours in data
data.loc[(data["area"] == 2) | (data["area"] == 3),"area"] = 1
data.loc[(data["area"].isna()),"area"] = 7
data.loc[(data["area"] == 6),"area"] = 0   #concentrate feeder to walk

test = data[["date","cowid","t","area"]].merge(sdata[["cowid","date","t","segment"]],
                                               copy = True,
                                               on = ["cowid","date","t"],
                                               )

# set area per segment area 0 = walk, area 1 = cubicle, area 4 = feed, area 5 = drink, area 6 = concentrate,  area 7 = unknown
sum_area = test[["segment","area","t"]].groupby(by = ["segment","area"]).count().reset_index()

sum_area = sum_area.sort_values(by=["segment","t"],ascending=False).reset_index(drop=1)
a = sum_area[["segment"]].drop_duplicates()
keep_segments = sum_area.iloc[a.index.values,:].sort_values(by = ["segment"]).reset_index(drop=1)

# calculate confidence based on % of values in that area for that segment
test2  = test[["segment","t"]].groupby(by = ["segment"]).count().reset_index()
keep_segments["confidence"] = keep_segments["t"]/test2["t"]*100
keep_segments = keep_segments.drop(columns = ["t"])

# add behaviour to sdata and save
behaviours = sdata.merge(keep_segments,
                         copy=True,
                         how= "outer",
                         on = ["segment"])


behaviours.to_csv(svpath + '\\behaviour_annotated_cows.txt')
