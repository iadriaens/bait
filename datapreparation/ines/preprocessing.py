# -*- coding: utf-8 -*-
"""
Created on Tue Nov 15 09:06:43 2022

@author: adria036
"""


import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\bait\scripts\bait\datapreparation\ines") 


#%% import packages

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# define functions

def accel_act(df,window):
    """
    combine the accelarations in the x,y and z dimension into a general activity
    paramter.
    Step 1 : rescale values in each direction
    Step 2 : set level of activtiy based on amplitude in preset window
    Step 3 : combine activity level in 3 directions
    
    df: Pandas DataFrame with columns 'acc_x', 'acc_y', 'acc_z','id', 'at'
    window: 
    
    """
    svpath = os.path.join("C:","/Users","adria036",
                        "OneDrive - Wageningen University & Research","iAdriaens_doc",
                        "Projects","iAdriaens","bait","results")
    
    try:
        df = df.rename(columns = {"cowid":"id"})
        namechange = 1
    except:
        namechange = 0
        
    # calculate activity
    ids = df["id"].drop_duplicates().reset_index(drop=1)
    output = pd.DataFrame([])
    for subj in ids:
        print("Calculating activity for subj = " + str(int(subj)))
        sset = df.loc[(df["id"]==subj) & \
                      (~df["acc_x"].isna()) & \
                      (~df["acc_y"].isna()) & \
                      (~df["acc_z"].isna()),:].copy()
        
        # standardise and scale measurements
        sset["acc_xs"] = (sset["acc_x"]-sset["acc_x"].min())/(sset["acc_x"].max()-sset["acc_x"].min())
        sset["acc_ys"] = (sset["acc_y"]-sset["acc_y"].min())/(sset["acc_y"].max()-sset["acc_y"].min())
        sset["acc_zs"] = (sset["acc_z"]-sset["acc_z"].min())/(sset["acc_z"].max()-sset["acc_z"].min())
        
        # set datetime to days, seconds, microseconds
        sset["d"] = sset["at"].dt.day-sset["at"].dt.day.min()
        sset["s"] = sset["at"].dt.hour*3600 \
                         + sset["at"].dt.minute*60 \
                         + sset["at"].dt.second
        # sset["ms"] = sset["at"].dt.microsecond/100
        
        # accumulated activity calculated in window: set window index
        sset["win"] = np.floor(sset["s"]/window)
        
        # accumulate activity in set windowsize - operation = std
        act = sset[["d","win","acc_xs","acc_ys","acc_zs"]].groupby(by=["d","win"]).std()
        act["acc_xs"] = act["acc_xs"].fillna(act["acc_xs"].mean()) # if only 1 measurement std= nan
        act["acc_ys"] = act["acc_ys"].fillna(act["acc_ys"].mean()) # replace by mean std
        act["acc_zs"] = act["acc_zs"].fillna(act["acc_zs"].mean())
        act["activity"] = (act["acc_xs"]+act["acc_ys"]+act["acc_zs"])/3
        act = act.reset_index()
        sset = pd.merge(sset,act[["d","win","activity"]],on = ["d","win"])
        
        
        # plot activity, standardized activity and time series of result
        # fig,ax = plt.subplots(nrows=1,ncols=3,figsize = (20,10))
        # ax[0].hist(sset["acc_x"],density=True,color = "g")
        # ax[1].hist(sset["acc_y"],density=True,color = "g")
        # ax[2].hist(sset["acc_z"],density=True,color = "g")
        # fig,ax = plt.subplots(nrows=1,ncols=3,figsize = (20,10))
        # ax[0].hist(sset["acc_xs"],density=True,color = "g")
        # ax[1].hist(sset["acc_ys"],density=True,color = "g")
        # ax[2].hist(sset["acc_zs"],density=True,color = "g")
        fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,10))
        T = 0
        for d in act["d"].drop_duplicates():
            ax.plot(act.loc[act["d"]==d,"win"],act.loc[act["d"]==d,"activity"]+T)
            T+=0.5
        ax.set_title("activity for subject " + str(int(subj)))
        ax.set_xlabel("time of the day, step = " + str(window) + "s")
        ax.set_ylabel("activity per day")
        plt.savefig(svpath + "\\activity_" + str(int(subj))+ ".tif")
        
        # combine data from all cows
        output = pd.concat([output,sset],axis = 0)
        
        # change name back to initial name
        if namechange == 1:
            output = output.rename(columns = {"id":"cowid"})
            
    return output 
  


#%% prepare dataset 1: accelerations (x,y,z) DC cows
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","data","nlas_ines")
svpath = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","preprocessing")
fn = "\\acceleration_dairy_campus.tif"


# read data
cows = pd.DataFrame([3152,1681,821,3106,419,3089,7387,1091,605],columns = ["cowid"])
data = pd.DataFrame([])
for f in os.listdir(path) :
    if ("bait" in f):
        print(f)
        
        new = pd.read_csv(path+"\\"+f,usecols=[0,4,7,8,9,10,11,14,16,18,19,20])
        new = pd.merge(new,cows,how="inner")
        new["at"] = pd.to_datetime(new["at"],format = "%Y-%m-%d %H:%M:%S.%f%z")
        new["day"] = new["at"].dt.day
        new["t"] = new["at"].dt.second + \
                   new["at"].dt.minute * 60 + \
                   new["at"].dt.hour * 3600
        
        # calculate average activity per second
        accindex = new[["cowid","acc_x","acc_y","acc_z","t","day"]].groupby(by = ["cowid","day","t"]).mean().reset_index()
        test = new[["cowid","t","day"]].drop_duplicates()
        new = new.iloc[test.index.values]
        new = new[["cowid","at","day","t","X","y","xnew","ynew"]]
        new = new.merge(accindex,
                        copy = True,
                        on = ["cowid","day","t"])
        
        # add to previous dataset
        data = pd.concat([data,new],axis=0)
    
del new, path, cows, f
del accindex

# visualisation of data: acceleration in x,y,z
plt.rc('font', size=14)
fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (20,18))
T = 0
yt = [T]
for ID in data["cowid"].drop_duplicates():
    ax.plot(data.loc[data["cowid"]==ID,"t"],data.loc[data["cowid"]==ID,"acc_x"]+T,linewidth=0.25,color="steelblue")
    ax.plot(data.loc[data["cowid"]==ID,"t"],data.loc[data["cowid"]==ID,"acc_y"]+T,linewidth=0.25,color="teal")
    ax.plot(data.loc[data["cowid"]==ID,"t"],data.loc[data["cowid"]==ID,"acc_z"]+T,linewidth=0.25,color="limegreen")
    ax.plot(data.loc[data["cowid"]==ID,"t"],T*np.ones(len(data.loc[data["cowid"]==ID,"t"])),linewidth=0.25,color="k")
    T+=data[["acc_x","acc_y","acc_z"]].max().max()+1
    yt.append(round(T))
ax.set_title("activity [m/s²]")
ax.set_xlabel("t [s]")
ax.set_ylabel("acceleration per cowid")
ax.legend(["acc_x","acc_y","acc_z"])
IDS = [str(ID) for ID in data["cowid"].drop_duplicates().astype(int).to_list()]
ax.set_yticks(yt[:-1],labels=IDS)
plt.savefig(svpath + fn)

del svpath, fn, fig, ax

# save without calculations of general activity level
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","data","preprocessed")
data.to_csv(path+"\\DCdata2.txt")


# calculate general activity level
df = data[["cowid","acc_x","acc_y","acc_z","at"]]
window = 60  # in seconds
df2 = accel_act(df,window)

# add activity to data
DCdata = pd.merge(data,df2[["cowid","at","window","activity"]],on = ["cowid","at"],how="outer")
del window, df, df2, data

# save to csv
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","data","preprocessed")
DCdata.to_csv(path+"\\DCdata.txt")
del path

# reformat as preferred for segmentation module (strip)



