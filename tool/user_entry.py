# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 12:18:46 2022

@author: adria036

-----------------------


input functions

"""

import wx, wx.adv

def onButton(event):
    print("Button pressed")

def get_dim():
 
    frame = wx.Frame(None, -1, 'win.py')
    frame.SetSize(-200,0,200,50)

    # Create text input
    dlg = wx.TextEntryDialog(frame, 'Enter the time series dimension:','Text Entry')
    dlg.SetValue("default")
    if dlg.ShowModal() == wx.ID_OK:
         print('You entered: %s\n' % dlg.GetValue())
    elif dlg.ShowModal() == wx.ID_CANCEL:
        print('User cancelled entry')
    dlg.Destroy()

    x = dlg.GetValue()
    
    return x

def get_minsize():
 
    frame = wx.Frame(None, -1, 'win.py')
    frame.SetSize(-200,0,200,50)

    # Create text input
    dlg = wx.TextEntryDialog(frame, 'Enter the minimum distance between changepoints:','Text Entry')
    dlg.SetValue("default")
    if dlg.ShowModal() == wx.ID_OK:
         print('You entered: %s\n' % dlg.GetValue())
    elif dlg.ShowModal() == wx.ID_CANCEL:
        print('User cancelled entry')
    dlg.Destroy()

    x = dlg.GetValue()
    
    return x

def get_id():
 
    frame = wx.Frame(None, -1, 'win.py')
    frame.SetSize(-200,0,200,50)

    # Create text input
    dlg = wx.TextEntryDialog(frame, 'Enter the id variable:','Text Entry')
    dlg.SetValue("cowid")
    if dlg.ShowModal() == wx.ID_OK:
         print('You entered: %s\n' % dlg.GetValue())
    elif dlg.ShowModal() == wx.ID_CANCEL:
        print('User cancelled entry')
    dlg.Destroy()

    x = dlg.GetValue()
    
    return x

def get_t():
 
    frame = wx.Frame(None, -1, 'win.py')
    frame.SetSize(-200,0,200,50)

    # Create text input
    dlg = wx.TextEntryDialog(frame, 'Enter the time variable:','Text Entry')
    dlg.SetValue("t")
    if dlg.ShowModal() == wx.ID_OK:
         print('You entered: %s\n' % dlg.GetValue())
    elif dlg.ShowModal() == wx.ID_CANCEL:
        print('User cancelled entry')
    dlg.Destroy()

    x = dlg.GetValue()
    
    return x


def get_model():
 
    frame = wx.Frame(None, -1, 'win.py')
    frame.SetSize(-200,0,200,50)

    # Create text input
    dlg = wx.TextEntryDialog(frame, 'Model for changepoint detection \n ' + 
                                    '"1" = l1, "2" = l2, "3" = normal','Text Entry')
    dlg.SetValue("3")
    if dlg.ShowModal() == wx.ID_OK:
         print('You entered: %s' % dlg.GetValue())
    elif dlg.ShowModal() == wx.ID_CANCEL:
        print('User cancelled entry')
    dlg.Destroy()

    x = int(dlg.GetValue())
    if x == 1:
        model = 'l1'
    elif x == 2:
        model = 'l2'
    else:
        model = 'normal'
    
    return model

def get_dataPath():
    frame = wx.Frame(None, -1, 'win.py')
    frame.SetSize(-200,0,200,50)

    # Create text input
    dlg = wx.TextEntryDialog(frame, 'Enter full path to data')
    dlg.SetValue("")
    if dlg.ShowModal() == wx.ID_OK:
         print('Data file: %s' % dlg.GetValue())
    elif dlg.ShowModal() == wx.ID_CANCEL:
        print('User cancelled entry')
    dlg.Destroy()
    
    x = dlg.GetValue()
    
    return x
    
def get_noFiles():
    frame = wx.Frame(None, -1, 'win.py')
    frame.SetSize(-200,0,200,50)

    # Create text input
    dlg = wx.TextEntryDialog(frame, 'Enter the number of files you want to use:','Text Entry')
    dlg.SetValue("1")
    if dlg.ShowModal() == wx.ID_OK:
         print('You entered: %s\n' % dlg.GetValue())
    elif dlg.ShowModal() == wx.ID_CANCEL:
        print('User cancelled entry')
    dlg.Destroy()

    x = dlg.GetValue()
    
    return x
    
def get_vars(dim):
    x = []
    for no in range(0,dim):
        frame = wx.Frame(None, -1, 'win.py')
        frame.SetSize(-200,0,200,50)

        # Create text input
        dlg = wx.TextEntryDialog(frame, 'Enter variable name ' + str(no+1) + ':','Text Entry')
        dlg.SetValue("acc_")
        if dlg.ShowModal() == wx.ID_OK:
            print('You entered: %s\n' % dlg.GetValue())
        elif dlg.ShowModal() == wx.ID_CANCEL:
            print('User cancelled entry')
        dlg.Destroy()
        vrs = dlg.GetValue()
        x.append(vrs)
    return x

def get_filenames(nofiles):
    x = []
    for no in range(0,nofiles):
        frame = wx.Frame(None, -1, 'win.py')
        frame.SetSize(-200,0,200,50)

        # Create text input
        dlg = wx.TextEntryDialog(frame, 'Enter filename ' + str(no+1) + ':','Text Entry')
        dlg.SetValue("")
        if dlg.ShowModal() == wx.ID_OK:
            print('You entered: %s\n' % dlg.GetValue())
        elif dlg.ShowModal() == wx.ID_CANCEL:
            print('User cancelled entry')
        dlg.Destroy()
        vrs = dlg.GetValue()
        x.append(vrs)
    return x


#%%
import wx

class choose(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, -1, "Dialog")
        mychoice = ['Select a directory','/home/rolf', '/home/public','/home/public/Documents']
        panel = wx.Panel(self,-1)
        select_dir = wx.Choice(panel,-1, choices=mychoice, pos=(20,20))
        self.Bind(wx.EVT_CHOICE, self.OnSelect)
        self.dir = mychoice[0]
        select_dir.SetSelection(0)
        self.Show()

    def OnSelect(self, event):
        if event.GetSelection() == 0:
            return
        self.dir = event.GetString()
        dlg = wx.FileDialog(None, message="Choose a file/files", defaultDir = self.dir, style=wx.FD_MULTIPLE)
        if dlg.ShowModal() == wx.ID_OK:
            print('Selected files are: ', dlg.GetPaths())
        dlg.Destroy()


if __name__ == '__main__':
    my_app = wx.App()
    choose(None)
    my_app.MainLoop()