# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 13:56:40 2022

@author: adria036

-------------------------------------------------------------------------------
Segmentation module of BAIT
- based on ruptures package;
- aims at segmenting one or multiple time series of (sensor) data in different 
  parts with different statistiacal characteristics. the features of this 
  segment can then be used to classify the a period of time into different 
  behaviours;
- depending on the behaviour of interest, a cost function of the segmentation 
  is chosen; based on this choice, segmentation of the TS is done based on, for
  example, the average, standard deviation, linear path, etc. of each segment;
- the input can be a single ts of measurements, or it can be combined into a 
  cost based on several ts together (e.g. x, y, z), potentially with a different
  weight;
- we recommend to use PELT (Pruned Linear Exact Time) as the search algorithm
  for the change points. PELT has a linear computational cost O(t), i.e., linear
  to the amount of measurements in the time series
- because of the cost of a search algorithm, larger series will have cost more
  time to segment;
- typically the exact amount of changes remains unknown, but can be guestimated 
  with the knowledge on the specific target behaviours. It is recommended to 
  estimate the amount of changes rather broadly, as this allows the classifier
  to decide for each segment whether or not the behaviour has changed;
- alternatively, a penalty function for adding a changepoint can be applied to 
  determine the amount of changes in the ts. However, when the variance across 
  behaviours is different, it typically is hard to rely on such penalty, as 
  the cost function minimization will optimize to changepoints in the area of 
  the largest variability;
- application of a minimal distance threshold between two changepoints can also
  be interesting; knowing that typically a 
- standardisation and application of a smoothing can always be of interest to
  mathematically optimize the search and avoid bias / local convergence caused
  by heteroscedasticity in the data
-------------------------------------------------------------------------------
General (mathematical) remarks on changepoint analysis
- commonly used is maximum likelihood estimation for change detection, in which
  the signal is modelled by variables with a piecewise constant distribution. 
  In this setting, changepoints detection equals maximum likelihood estimation 
  with the sum of the cost equal to the negative log-likelihood. The distributions
  need to be known (prior knowledge) and the model is either a change in mean or
  a change in mean and scale (not with real heteroscedastic data: mean and std
  multiplied with the same factor?)
- the computational cost is proportional to the number of data points in the ts
  this means that it is wise to cut you dataset in smaller meaningful windows 
  before calculating the breakpoints, which can afterwards be combined again for
  the classification algorithms
- outliers influence the estimation of cost tremendously. Where possible, try 
  to reduce the outliers before estimating the changepoints. 

-------------------------------------------------------------------------------
RUPTURES - COST FUNCTIONS



RUPTURES - BASE FUNCTIONS
* error(start,end) -- returns the cost on segment (start,end)
* fit(*args,**kwargs)  -- set parameters of the 

RUPTURES - SEARCH METHODS
* PELT: Pruned Linear Exact Cost
    -
    -
    -
    -


"""


import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\bait\scripts\bait\tool") 
#%matplotlib qt



#%% user inputs
# from user_entry import get_dim, get_minsize, get_model, get_id, get_noFiles, get_t, get_vars, get_filenames,get_dataPath


# dim = int(get_dim())            # dimensionality of the time series
# min_size = int(get_minsize())   # minimum distance between changepoints (in measurements)
# model = get_model()             # model = l1 (level), l2 (level+scale) or normal (normal) 
# idname = get_id()               # name of variable containing subject ids
# tname = get_t()                 # name of variable containing time stamp
# variable = get_vars(dim)        # name of segmentation variables ~dim
# nofiles = int(get_noFiles())    # number of files to load
# path_data = get_dataPath()      # datapath 
# fn = get_filenames(nofiles)     # filenames
# medfilter = 0                   # set medfilter parameter




#%% filepaths, constants and load data

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import ruptures as rpt
from scipy.signal import medfilt
from datetime import timedelta

# path to data
# path = os.path.join("C:","/Users","adria036",
#                     "OneDrive - Wageningen University & Research","iAdriaens_doc",
#                     "Projects","iAdriaens","bait","data","preprocessed")
# path to data SP
path = os.path.join("W:","ASG","WLR_Dataopslag","Genomica","Sustainable_breeding",
                    "4400003586_BAIT","Dataset_Ines_Marja")

fns = [f for f in os.listdir(path) \
        if os.path.isfile(os.path.join(path,f)) \
            and ("newseldata_cow_" in f) \
            and ("_2022" not in f)]
fns = pd.DataFrame(fns,columns = ["fn"])
fns["cowid"] = fns["fn"].iloc[:].str[15:-4].astype(int)

# read dataset accelerations
# data = pd.read_csv(path+"\\DCdata2.txt", index_col=0)
# data["at"] = pd.to_datetime(data["at"],format = "%Y-%m-%d %H:%M:%S.%f%z")

# read dataset 
# data = pd.read_csv(path+"\\DCdata.csv", index_col=0)
# data["at"] = pd.to_datetime(data["at"],format = "%Y-%m-%d %H:%M:%S.%f%z")
# # #data = data[["cowid","at","t","gap","activity"]]

# # dataset sheep
# data = pd.read_csv(path+"\\HDdata_sheep1_back.csv", index_col=0)
# data["at"] = pd.to_datetime(data["at"],format = "%Y-%m-%d %H:%M:%S")

# read data  SP
for f in fns["fn"]:
    print(f)
# fn = "newseldata_cow_280.txt"
    data = pd.read_csv(os.path.join(path,f), 
                       usecols = ["cowid","barn","date","t","acc_xs","acc_ys","acc_zs"] )
    data["date"]= pd.to_datetime(data["date"],format = "%Y-%m-%d")
    data["datetime"] = data["date"] + \
        (pd.to_datetime(data["t"],unit = 's') - pd.to_datetime("1970-01-01",format = "%Y-%m-%d"))
    dcol = "date"
    data["day"] = (data["date"] - data["date"].min()).dt.days

    # plot %matplotlib qt  ---- this is and example
    # fig,ax = plt.subplots(nrows=3,ncols=1,figsize = (20,10),sharex=True)
    # data2 = data.loc[data[dcol].dt.day==27,:].sort_values(by="t").reset_index(drop=1)
    # ax[0].plot(data2["t"],data2["acc_xs"],color = "lightgrey")
    # ax[1].plot(data2["t"],data2["acc_ys"],color = "teal")
    # ax[2].plot(data2["t"],data2["acc_zs"],color = "teal")
    #ax[0].plot(data2["t"],data2["acc_xs"].rolling(10).median(),color = "k")
    #ax[1].plot(data2["t"],data2["acc_ys"].rolling(10).median(),color = "k")
    #ax[2].plot(data2["t"],data2["acc_zs"].rolling(10).median(),color = "k")

    # del path

    #%% segmentation tests
    # savepath
    # path = os.path.join("C:","/Users","adria036",
    #                     "OneDrive - Wageningen University & Research","iAdriaens_doc",
    #                     "Projects","iAdriaens","bait","results","segmentation")
    svpath = os.path.join("W:","ASG","WLR_Dataopslag","Genomica","Sustainable_breeding",
                        "4400003586_BAIT","Dataset_Ines_Marja","segmentation")
    """
    # define parameters for segmentation
    
    # set minimum time instead of minimum size and set signal
    """
    model = "normal"
    min_size = 2*60  # minimum distance between changepoints = in seconds
    dim = 3
    # variable = ["acc_x","acc_y","acc_z"]
    variable = ["acc_xs","acc_ys","acc_zs"]
    idname = "cowid"
    tname = "datetime"
    medfilter = 1
    # select data and perform segmentation
    
    allids = data[idname].drop_duplicates()
    nseg = 1
    brkpnts = pd.DataFrame([])
    output = pd.DataFrame([])
    for ids in allids:
        days = data["day"].drop_duplicates()
        for day in days:
            # select data
            signal = data.loc[(data[idname]==ids) & \
                      (data["day"] == day) \
                      ,variable]
            signal = signal.dropna()
            indx = signal.index.values
            signal = signal.to_numpy()
            if len(signal) > 3600:
            
                # filter data to remove noise / errors
                if (medfilter == 1): 
                    for d in range(0,signal.shape[1]):
                        mfilt = medfilt(signal[:,d],5)
                        signal[:,d] = mfilt
            
                # set penalty values // rule of thumb = log(n)*dim*std(signal)
                #pen = np.log(len(signal)) * dim * np.std(signal, axis=0).mean()**2
                pen = np.log(len(signal)) * dim * np.std(signal)**2
                # if pen < 700:
                #      pen = 950
                pen = 350   # for the SP data, this seems a generally good threshold
                
                print("id = " + str(ids))
                print("pen = " + str(pen))
                print("day = " + str(day))
                # if segment std ~= signal std-  this might not work
                
            
                # fit and define changepoint model
                # algo = rpt.Pelt(model=model,min_size=min_size).fit(signal)
                
                c = rpt.costs.CostNormal().fit(signal)
                algo = rpt.Pelt(custom_cost=rpt.costs.CostNormal(),min_size=min_size).fit(signal)
                #algo = rpt.Pelt(model="rbf",min_size=min_size).fit(signal)
                cpts = algo.predict(pen = pen)
               
                # save breakpoints in new variable
                df = pd.DataFrame(np.linspace(nseg,nseg+len(cpts)-1,len(cpts)).astype(int),columns=["segment"])
                df["start"] = [0]+cpts[:-1]
                df["end"] = cpts[:-1]+[len(signal)]
                df["end"] = df["end"]-1
                nseg = nseg + len(cpts)
                
                # initiate figure
                fig, axes = rpt.display(signal, cpts,figsize = (18,9))
                
                # add breakpoints / segments to signal
                signal = pd.DataFrame(signal,columns = variable)
                signal["segment"] = np.nan
                signal.iloc[df["start"].values,signal.columns.get_indexer(["segment"])] = df["segment"].values
                signal.iloc[df["end"].values,signal.columns.get_indexer(["segment"])]  = df["segment"].values
                signal = signal.fillna(method="pad")
                
                signal.index = indx
                signal["at"] = data.loc[(data[idname]==ids) & \
                          (data[tname].dt.day == day) & \
                          (~data[tname].isna()),dcol]
                signal["date2"] = signal["at"].dt.strftime("%d/%m %H:%M")
                signal["id"] = ids
                
                # plot
                axes[0].set_title("cow = " + str(int(ids)) + ", day = " +str(day) +   "acceleration in x direction")
                axes[1].set_title("acceleration in y direction")
                axes[2].set_title("acceleration in z direction")
                
                axes[0].set_ylabel("acceleration [m/s²]")
                axes[1].set_ylabel("acceleration [m/s²]")
                axes[2].set_ylabel("acceleration [m/s²]")
                axes[2].set_xlabel("measurement no.")
                
                plt.tight_layout()
                plt.savefig(svpath+"\\add_segm_cow_"+str(round(ids))+"_day"+str(round(day))+".tif")
                plt.close()
                    
                
                # add signal to output and add df to brkpnts
                output = pd.concat([output,signal])
                brkpnts = pd.concat([brkpnts,df])
            
    output.to_csv(svpath + "\\SP_cow_" + str(ids) + "_segmented_2min.txt")
    brkpnts.to_csv(svpath+"\\SP_cow_" + str(ids) + "_breakpoints_2min.txt")


