# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 10:31:27 2022

@author: adria036



-------------------------------------------------------------------------------
CLASSIFICATION MODULE BAIT
- based on scikit learn package;
- classification of segments from segmentation module with ML classifier
- uses segments statistical characteristics

-------------------------------------------------------------------------------
per segment, following features can be calculated:
    - mean/level each dimension
    - range  each dimension
    - Q25-Q75 - IQR each dimension
    - std each dimension
    - length of the segment
    - skewness
    - gapsize
    - % outliers
    - mean/level each dimension of the previous segment
    - range each dimension of the previous segment
    - Q25-Q75 - IQR each dimension of the previous segment
    - std each dimension of the previous segment
    - length of the segment of the previous segment
    - % outliers of the previous segment
"""

import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.integrate import trapz
from scipy.fft import rfft, rfftfreq
import seaborn as sns
#%matplotlib qt

#%% for user interface: decide which features are expected to differ across the categories of interest
"""
Calculated: 
    - level
    - std
    - iqr
    - integral of frequency domein components (freq > 0) calc.
    - skewness
    - kurtosis
"""

usr_feat = ["level", "std", "iqr", "freq"]
data_standardize = True

#%% load data

path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","segmentation")

data = pd.read_csv(path + "//HD_sheep1_segmented_2min.csv", index_col=0)
data["at"] =  pd.to_datetime(data["at"],format = "%Y-%m-%d %H:%M:%S")
del path

# data.rename(columns={"acc_xm":"acc_x","acc_ym":"acc_y","acc_zm":"acc_z"},inplace=True)
# data=data.sort_values(by = "at").reset_index(drop=1)

# set dimensionality of the data
variables = ["acc_xm","acc_ym","acc_zm"]
dim = len(variables)

# standardise the data
if data_standardize == True:
    for var in variables:
        data[var] = (data[var] - data[var].min()) / (data[var].max()-data[var].min())

    del var


#%% define features

# prepare dataframe
segments = pd.DataFrame([],index = data["segment"].drop_duplicates().values.astype(int))
segs = data["segment"].drop_duplicates().values

# calculate extra vars for feature design
data["timediff"] = data["at"].diff()
data["timediff"] = data["timediff"].dt.seconds

# calculate features
segments["duration"] = data[["segment",variables[0]]].groupby(by="segment").count()
# segments["gap"] = data[["segment","timediff"]].groupby(by="segment").max()
for i in range(0,dim):
    segments["level_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").mean().values
    segments["std_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").std().values
    #segments["range_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").max().values - \
    #                            data[["segment",variables[i]]].groupby(by="segment").min().values
    segments["iqr_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").describe().values[:,6] - \
                              data[["segment",variables[i]]].groupby(by="segment").describe().values[:,4]
    segments["skew_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").skew()
    segments["kurt_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").apply(pd.DataFrame.kurt).values[:,1]
    
    segments["freq_"+str(i)] = np.nan                                                
    for seg in segs:
        yf = np.abs(rfft(data.loc[data["segment"]==seg,variables[i]].values))
        xf = rfftfreq(len(data.loc[data["segment"]==seg]),1)
                
        # calculate integral of frequencies > 0
        segments["freq_"+str(i)].iloc[int(seg)-1] = round(trapz(yf[1:],xf[1:]),3)
        
del xf, yf, seg, segs, i
                            

# if features too unbalanced, clustering learns the feature size instead of cluster difference
for i in range(0,segments.shape[1]):
    segments.iloc[:,i] = (segments.iloc[:,i] - segments.iloc[:,i].min()) / \
        (segments.iloc[:,i].max() - segments.iloc[:,i].min())


# explore (relation between) features
matrix = segments.corr()            # correlation matrix
summary = segments.describe()       # summary

fig, ax = plt.subplots(nrows=1,ncols=2, figsize=(14,6))
bp = sns.boxplot(segments,ax= ax[0], palette ="viridis",fliersize = 2)
if segments.shape[1] < 10:
    ax[1] = sns.heatmap(segments.corr(), cmap = "RdBu",annot = True, fontsize = 6)
else:
    ax[1] = sns.heatmap(segments.corr(), cmap = "RdBu",annot = False)
ax[0].set_ylabel("Standardised feature values")
ax[0].set_xlabel("Feature")
ax[0].set_title("Feature distribution")
ax[1].set_title("Feature correlations")
ax[0].set_xticklabels(ax[0].get_xticklabels(),rotation=45)
ax[1].set_xticklabels(ax[1].get_xticklabels(),rotation=45)


del i, fig, ax

#%% test fouriertransformation



path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","classification")

selseg = [766,767,768,769,770,771,772,773]
segACT = ["A","I","A","I","IIA","I","IAA","A"]
subset = data.loc[(data["segment"].isin(selseg)),:]


for i in range(0,len(selseg)):
    yf = np.abs(rfft(subset.loc[subset["segment"]==selseg[i],"acc_xm"].values))
    xf = rfftfreq(len(subset.loc[subset["segment"]==selseg[i]]),1)
    
    # calculate length of the signal
    L = len(subset.loc[subset["segment"]==selseg[i]])
    total = sum(yf)   # this is the sum of all the data (dependent on level)
    
    # calculate integral of frequencies > 0
    I_trapz = trapz(yf[1:],xf[1:])
        
    fig,ax = plt.subplots(nrows=1,ncols=1,figsize = (5,4.5))
    ax.plot(xf[1:], yf[1:]/(len(subset.loc[subset["segment"]==selseg[i]])))
    ax.plot(xf[1:],np.ones(len(xf[1:]))*0.002,'--',color='r')
    ax.set_title("segment = " + str(selseg[i]) + ", behaviour = " + segACT[i] + \
                 ", frequency integral = " +str(round(I_trapz,3)) \
                 ,fontsize = 8)
    ax.set_ylim([0,0.05])
    print("selseg = " + str(selseg[i]) + ", behaviour = " + segACT[i] +", freq. int = " + str(round(I_trapz,3)))
    fn = "freq_segment_" + str(selseg[i]) + ".png"
    plt.savefig(path + "\\" + fn)


#%% unsupervised learning
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
# from sklearn.decomposition import PCA

path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","segmentation")

flist = []
for idx in range(0,segments.shape[1]):
    ad = [segments.columns[idx] for m in usr_feat if m in segments.columns[idx]]
    if len(ad) > 0: 
        flist.append(ad[0])

for i in range(2,3):
    segments["cluster"] = KMeans(init="k-means++", n_clusters=i, n_init=4, random_state=0).fit_predict(segments[flist])
    
    try:
        data = data.drop(columns = "cluster")
    except:
        print("cluster not removed")
    
    # add to data for plot
    data = pd.merge(data,segments["cluster"], how = "outer",
                    left_on = "segment", right_index = True)
    
    # check distance between cluster means
    clusters = segments.groupby(by="cluster").mean()
    
    
    # make figure
    for day in [1,6,29]:  #data["at"].dt.day.drop_duplicates()
        print(day)
        data2 = data.loc[data["at"].dt.day==day,:].sort_values(by="at").reset_index(drop=1)
    
        fig,ax = plt.subplots(nrows=dim,ncols=1,figsize = (20,10),sharex=True)
        ax[0].plot(data2["at"],data2["acc_xm"],color = "teal")
        ax[1].plot(data2["at"],data2["acc_ym"],color = "teal")
        ax[2].plot(data2["at"],data2["acc_zm"],color = "teal")
        cols = ["red","blue","seagreen","tan","white"]
        for segment in data2["segment"].drop_duplicates():
            subset = data2.loc[data2["segment"] == segment,["at","cluster"]].reset_index(drop=1)
            ax[0].text(subset["at"][0],0.8,round(segment),fontsize = "xx-small")
            test = ax[0].fill_between(subset["at"], 
                               np.ones_like(subset["cluster"])*data["acc_xm"].min(),
                               np.ones_like(subset["cluster"])*data["acc_xm"].max(), 
                               color = cols[int(subset["cluster"][0])],alpha=0.2,
                               edgecolor = None)
            ax[1].fill_between(subset["at"], 
                               np.ones_like(subset["cluster"])*data["acc_ym"].min(),
                               np.ones_like(subset["cluster"])*data["acc_ym"].max(), 
                               color = cols[int(subset["cluster"][0])],alpha=0.2,edgecolor = None)
            ax[2].fill_between(subset["at"], 
                               np.ones_like(subset["cluster"])*data["acc_zm"].min(),
                               np.ones_like(subset["cluster"])*data["acc_zm"].max(), 
                               color = cols[int(subset["cluster"][0])],alpha=0.2,edgecolor = None)
        
    
        ax[0].set_title("No. clusters = " + str(i) + ", day = " + str(day) )
        ax[2].set_xlabel("datetime")
        ax[0].set_ylabel("acc_x [m/s²]")
        ax[1].set_ylabel("acc_y [m/s²]")
        ax[2].set_ylabel("acc_z [m/s²]")
        
        fn = "cluster_" + str(i) + "_day" + str(day) + ".png"
        
        #plt.savefig(path + "\\" + fn)
        #plt.close()
    


#%% unsupervised learning - gaussian mixtures

import numpy as np
import matplotlib.pyplot as plt
from sklearn.mixture import GaussianMixture
# from sklearn.decomposition import PCA

path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","classification")

flist = []
for idx in range(0,segments.shape[1]):
    ad = [segments.columns[idx] for m in usr_feat if m in segments.columns[idx]]
    if len(ad) > 0: 
        flist.append(ad[0])

for i in range(2,3):
    
    segments["GM_cluster"] = GaussianMixture(n_components = i, random_state = 0).fit_predict(segments[flist])
    
    #segments["cluster"] = KMeans(init="k-means++", n_clusters=i, n_init=4, random_state=0).fit_predict(segments[flist])
    
    try:
        data = data.drop(columns = "GM_cluster")
    except:
        print("GM_cluster not removed")
        
     # try:
     #     data = data.drop(columns = "cluster")
     # except:
     #     print("cluster not removed")
    
    # add to data for plot
    data = pd.merge(data,segments["GM_cluster"], how = "outer",
                    left_on = "segment", right_index = True)
    
    # check distance between cluster means
    clusters = segments.groupby(by="GM_cluster").mean()
    
    
    # make figure
    for day in data["at"].dt.day.drop_duplicates():  #[1,6]
        print(day)
        data2 = data.loc[data["at"].dt.day==day,:].sort_values(by="at").reset_index(drop=1)
        sumclust = data2[["GM_cluster","at"]].groupby(by = "GM_cluster").count()
        sumclust = (sumclust.values/sum(sumclust["at"])*100).round(2)
        
        fig,ax = plt.subplots(nrows=dim,ncols=1,figsize = (20,10),sharex=True)
        ax[0].plot(data2["at"],data2["acc_xm"],color = "teal")
        ax[1].plot(data2["at"],data2["acc_ym"],color = "teal")
        ax[2].plot(data2["at"],data2["acc_zm"],color = "teal")
        cols = ["red","blue","seagreen","tan","white"]
        for segment in data2["segment"].drop_duplicates():
            subset = data2.loc[data2["segment"] == segment,["at","GM_cluster"]].reset_index(drop=1)
            ax[0].text(subset["at"][0],0.8,round(segment),fontsize = "xx-small")
            test = ax[0].fill_between(subset["at"], 
                               np.ones_like(subset["GM_cluster"])*data["acc_xm"].min(),
                               np.ones_like(subset["GM_cluster"])*data["acc_xm"].max(), 
                               color = cols[int(subset["GM_cluster"][0])],alpha=0.2,
                               edgecolor = None)
            ax[1].fill_between(subset["at"], 
                               np.ones_like(subset["GM_cluster"])*data["acc_ym"].min(),
                               np.ones_like(subset["GM_cluster"])*data["acc_ym"].max(), 
                               color = cols[int(subset["GM_cluster"][0])],alpha=0.2,edgecolor = None)
            ax[2].fill_between(subset["at"], 
                               np.ones_like(subset["GM_cluster"])*data["acc_zm"].min(),
                               np.ones_like(subset["GM_cluster"])*data["acc_zm"].max(), 
                               color = cols[int(subset["GM_cluster"][0])],alpha=0.2,edgecolor = None)
        ncl = ""
        for j in range(0,i):
            ncl = ncl + " C" + str(j) + " = " + str(sumclust[j][0])  + "% "
        ax[0].set_title("GM -- No. clusters = " + str(i) + ", day = " + str(day) + "," + ncl)
        ax[2].set_xlabel("datetime")
        ax[0].set_ylabel("acc_x [m/s²]")
        ax[1].set_ylabel("acc_y [m/s²]")
        ax[2].set_ylabel("acc_z [m/s²]")
        
        fn = "GMcluster_" + str(i) + "_day" + str(day) + ".png"
        
        plt.savefig(path + "\\" + fn)
        plt.close()
    

