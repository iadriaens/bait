# -*- coding: utf-8 -*-
"""
Created on Wed Jan 11 10:01:13 2023

@author: adria036
------------------------------------------------------------------------------

Classification based on supervised learning of segment features
per segment, following features can be calculated:
    - mean/level each dimension
    - range  each dimension
    - Q25-Q75 - IQR each dimension
    - std each dimension
    - length of the segment
    - skewness
    - gapsize
    - % outliers
    - mean/level each dimension of the previous segment
    - range each dimension of the previous segment
    - Q25-Q75 - IQR each dimension of the previous segment
    - std each dimension of the previous segment
    - length of the segment of the previous segment
    - % outliers of the previous segment




"""


import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.integrate import trapz
from scipy.fft import rfft, rfftfreq
from sklearn.model_selection import train_test_split
import seaborn as sns
from random import sample
#%matplotlib qt


#%% for user interface: decide which features are expected to differ across the categories of interest
"""
Calculated: 
    - level
    - std
    - iqr
    - integral of frequency domein components (freq > 0) calc.
    - skewness
    - kurtosis
"""

# set hyperparameters and settings
settings = {"user_feat" : ["level", "std", "iqr", "freq"],
            "data_standardize" : True,
            "crossval" : 5,
            "data_split_by" : "time",       # "time" or "random"
            "data_split_days" : 5,          # number of days to train on if "time"
            "data_partitioning" : 90,       # % randomly selected data if "random"
            "ground_truth" : "area",        # colname of ground truth e.g. "Behaviour" or "area"
            "id" : "cowid",                 # id col name
            "variables" : ["acc_x","acc_y","acc_z"] # variable names
            }  

# TODO: in models if num classes more than two no logistic regression
# TODO: try ensembles
# TODO: feature selection step in crossvalidation


#%% load data

path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","preprocessed","cow")

data = pd.read_csv(path + "//behaviour_annotated_cows.txt", index_col=0)
data["at"] =  pd.to_datetime(data["at"],format = "%Y-%m-%d %H:%M:%S")
del path

# with too few ground truth for class/area 7 and 5 PER COW: set to 0 (walk)
data.loc[((data["area"] == 7)  | (data["area"] == 5)),"area"] = 0


# set dimensionality of the data
dim = len(settings["variables"])
variables = settings["variables"]

# standardise the data
if settings["data_standardize"] == True:
    for var in variables:
        data[var] = (data[var] - data[var].min()) / (data[var].max()-data[var].min())

    del var

# set output path
path_out =  os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","iAdriaens","bait","results","classification","cow")


#%% calculate features

# prepare dataframe
segments = pd.DataFrame([],index = data["segment"].drop_duplicates().values.astype(int))
segs = data["segment"].drop_duplicates().values

# calculate extra vars for feature design
data["timediff"] = data["at"].diff()
data["timediff"] = data["timediff"].dt.seconds

# calculate features
if "dur" in settings["user_feat"]:
    segments["duration"] = data[["segment",variables[0]]].groupby(by="segment").count()
    
# segments["gap"] = data[["segment","timediff"]].groupby(by="segment").max()
for i in range(0,dim):
    if "level" in settings["user_feat"]:
        segments["level_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").mean().values
    if "std" in settings["user_feat"]:
        segments["std_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").std().values
    if "range" in settings["user_feat"]:
        segments["range_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").max().values - \
                                data[["segment",variables[i]]].groupby(by="segment").min().values
    if "iqr" in settings["user_feat"]:
        segments["iqr_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").describe().values[:,6] - \
                              data[["segment",variables[i]]].groupby(by="segment").describe().values[:,4]
    if "skew" in settings["user_feat"]:
        segments["skew_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").skew()
    if "kurt" in settings["user_feat"]:
        segments["kurt_"+str(i)] = data[["segment",variables[i]]].groupby(by="segment").apply(pd.DataFrame.kurt).values[:,1]
    
    if "freq" in settings["user_feat"]:
        segments["freq_"+str(i)] = np.nan                                                
        for seg in segs:
            yf = np.abs(rfft(data.loc[data["segment"]==seg,variables[i]].values))
            xf = rfftfreq(len(data.loc[data["segment"]==seg]),1)
                
            # calculate integral of frequencies > 0
            segments["freq_"+str(i)].iloc[int(seg)-1] = round(trapz(yf[1:],xf[1:]),3)
        del xf, yf, seg
del segs, i                        

# if features too unbalanced, clustering learns the feature size instead of cluster difference
for i in range(0,segments.shape[1]):
    segments.iloc[:,i] = (segments.iloc[:,i] - segments.iloc[:,i].min()) / \
        (segments.iloc[:,i].max() - segments.iloc[:,i].min())

#------------------------------------------------------------------------------
# explore (relation between) features
matrix = segments.corr()            # correlation matrix
summary = segments.describe()       # summary

fig, ax = plt.subplots(nrows=1,ncols=2, figsize=(14,6))
bp = sns.boxplot(segments,ax= ax[0], palette ="viridis",fliersize = 2)
if segments.shape[1] < 10:
    ax[1] = sns.heatmap(segments.corr(), cmap = "RdBu",annot = True, fontsize = 6)
else:
    ax[1] = sns.heatmap(segments.corr(), cmap = "RdBu",annot = False)
ax[0].set_ylabel("Standardised feature values")
ax[0].set_xlabel("Feature")
ax[0].set_title("Feature distribution")
ax[1].set_title("Feature correlations")
ax[0].set_xticklabels(ax[0].get_xticklabels(),rotation=45)
ax[1].set_xticklabels(ax[1].get_xticklabels(),rotation=45)


del i, fig, ax
del bp


flist = []
for idx in range(0,segments.shape[1]):
    ad = [segments.columns[idx] for m in settings["user_feat"] if m in segments.columns[idx]]
    if len(ad) > 0: 
        flist.append(ad[0])

#------------------------------------------------------------------------------
# make box plots of features per dimension and per class in "truth"
fig,axes = plt.subplots(ncols = dim, nrows = len(settings["user_feat"]), 
                   figsize = (5*dim,4*len(settings["user_feat"])), sharex = True)
beh = ["walk","cubicle","feed"]
for i in range(len(settings["user_feat"])):
    for j in range(dim):
        colname = settings["user_feat"][i] + '_' + str(j)
        print(colname)
        sns.boxplot(x = "truth", y = colname, data = segments, ax = axes[i][j])
        if i == len(settings["user_feat"])-1:
            axes[i][j].set_xticklabels(beh,rotation=0)
        if i == 0:
            axes[i][j].set_title(settings["variables"][j])

#------------------------------------------------------------------------------
# bar plots per behaviour per cow per day (number of segments)
days = segments["day"].drop_duplicates().reset_index(drop=1)
ids = segments["id"].drop_duplicates().reset_index(drop=1)

# add behaviour to segments
for j in range(len(ids)):
    fig, axes = plt.subplots(ncols = 1,
                             nrows = 2, 
                       figsize = (20,15))
                       
    subset = segments.loc[segments["id"] == ids[j],["day","truth","id"]]
    subset1 = subset.groupby(by = ["day","truth"]).count().reset_index()
    subset2 = subset[["day","id"]].groupby(by = ["day"]).count().reset_index()
    dfcol = pd.merge(subset1,subset2,how = "outer", on = "day")
    dfcol["perc"] = dfcol["id_x"]/dfcol["id_y"]*100
    sns.barplot(data = dfcol, x = "day" , y = "perc", palette = "rocket", hue = "truth", ax = axes[0])
    h, l = axes[0].get_legend_handles_labels()
    axes[0].legend(h, beh, title="Behaviour")
    axes[0].set_title("cow " + str(round(ids[j])))
    axes[0].set_ylabel("percentage of segments in gold standard")
    sns.barplot(data = dfcol, x = "day" , y = "id_x", palette = "rocket", hue = "truth", ax = axes[1])
    h, l = axes[1].get_legend_handles_labels()
    axes[1].legend(h, beh, title="Behaviour")
    axes[1].set_title("cow " + str(round(ids[j])))
    axes[1].set_ylabel("number of segments in gold standard")
    plt.savefig(path_out + "//overview_behaviour_cow_" + str(round(ids[j])))
    plt.close()

del ids, days

#%% data split


# add ground truth to "segments" data frame
gs_index = data["segment"].drop_duplicates().index.values
segments["truth"] = data[settings["ground_truth"]].iloc[gs_index].values
segments["day"] = data["day"].iloc[gs_index].values
segments["id"] = data[settings["id"]].iloc[gs_index].values
del gs_index

if "time" in settings["data_split_by"]:
    
    # for each cow, select the first settings["data_split_days"] days as the training
    df = {"train" : segments.loc[segments["day"] <= settings["data_split_days"],:],
          "test" : segments.loc[segments["day"] > settings["data_split_days"],:]}  
    
else:    
    # use sklearn model selection train_test_split
    X_train, X_test = train_test_split(segments,
         test_size = 1-(settings["data_partitioning"]/100), 
         stratify = segments[["id","truth"]] 
         )   # stratify for behaviours
    X_train = X_train.sort_index(axis=0)    
    X_test = X_test.sort_index(axis=0)
    
    # put in dictionary    
    df = {"train" : X_train,
          "test" : X_test}
    
    del X_train, X_test

#------------------------------------------------------------------------------
# TODO: explore test and train datasets created -- overall
# TODO: explore test and train datasets created -- per individual id
# !!! set feature list

    







#%% supervised learning for all ids in the dataset

from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

model_pipeline = []
model_pipeline.append(SVC())
model_pipeline.append(KNeighborsClassifier())
model_pipeline.append(DecisionTreeClassifier())
model_pipeline.append(RandomForestClassifier())
model_pipeline.append(GaussianNB())

#Model running and evaluation
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

model_list = ["SVM", "KNN","Decision Tree", "Random Forest", "Naive Bayes"]
acc_list = []
auc_list = []
cm_list = []



for model in model_pipeline:
    print(model)
    model.fit(df["train"][flist], df["train"]["truth"])
    y_pred = model.predict(df["test"][flist])
    acc_list.append(metrics.accuracy_score(df["test"]["truth"], y_pred))
    #fpr, tpr, _thresholds = metrics.roc_curve(np.array(y_test), y_pred)
    #auc_list.append(round(metrics.auc(fpr, tpr), 2))
    cm_list.append(confusion_matrix(df["test"]["truth"], y_pred))        

#visualise confusion matrix
fig = plt.figure(figsize = (15,10))
for i in range(len(cm_list)):
    print(i)
    cm = cm_list[i]
    model = model_list[i]
    sub = fig.add_subplot(2,3,i+1).set_title(model)
    cm_plot = sns.heatmap(cm, annot = True, cmap = 'Blues_r')
    cm_plot.set_xlabel('Predicted Values')
    cm_plot.set_ylabel('Actual Values')

# Summarize accuracy
result_df = pd.DataFrame({'Model': model_list, 'Accuracy': acc_list})



#%% supervised learning per individual id in the dataset

from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

model_pipeline = []
model_pipeline.append(SVC())
model_pipeline.append(KNeighborsClassifier())
model_pipeline.append(DecisionTreeClassifier())
model_pipeline.append(RandomForestClassifier())
model_pipeline.append(GaussianNB())

#Model running and evaluation
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

model_list = ["SVM", "KNN","Decision Tree", "Random Forest", "Naive Bayes"]
acc_list = []
auc_list = []
cm_list = []

# set feature list
flist = []
for idx in range(0,segments.shape[1]):
    ad = [segments.columns[idx] for m in settings["user_feat"] if m in segments.columns[idx]]
    if len(ad) > 0: 
        flist.append(ad[0])

for model in model_pipeline:
    model.fit(df["train"][flist], df["test"]["truth"])
    y_pred = model.predict(x_test)
    acc_list.append(metrics.accuracy_score(y_test, y_pred))
    #fpr, tpr, _thresholds = metrics.roc_curve(np.array(y_test), y_pred)
    #auc_list.append(round(metrics.auc(fpr, tpr), 2))
    cm_list.append(confusion_matrix(y_test, y_pred))        

#visualise confusion matrix
fig = plt.figure(figsize = (15,10))
for i in range(len(cm_list)):
    cm = cm_list[i]
    model = model_list[i]
    sub = fig.add_subplot(2,3,i+1).set_title(model)
    cm_plot = sns.heatmap(cm, annot = True, cmap = 'Blues_r')
    cm_plot.set_xlabel('Predicted Values')
    cm_plot.set_ylabel('Actual Values')

# Summarize accuracy
result_df = pd.DataFrame({'Model': model_list, 'Accuracy': acc_list})
